#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "../include/extra_functions.h"
#include "../include/file_functions.h"
#include "../include/structs.h"


int main(int argCount, char* args[]){
    struct BMPInfo info;
    struct image image;
    struct image imageOut;
    struct BMPInfo infoForNew;
    if (argCount!=3)
    {
        return 1;
    }
    const char* inputFilePath = args[1];
    const char* outputFilePath = args[2];


    read_BMP_file(inputFilePath, &info, &image);

    rotate(&image, &imageOut, &info, &infoForNew);

    write_data_in_file(outputFilePath, &infoForNew, &imageOut);
    return 0;
    
}