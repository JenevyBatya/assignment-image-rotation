#include "../include/file_functions.h"
#include "../include/extra_functions.h"
#include <stdio.h>
#include <stdlib.h>

//Компилится хорошо, пока нареканий нет
// Функция чтения файла
int read_BMP_file(const char* input_file_path, struct BMPInfo* info, struct image* image) {
    //Открываем файл
    FILE* file = fopen(input_file_path, "rb");
    if (file == NULL) {
        return 1;
    }
//Считываем основную информацию по файлу
    fread(info, sizeof(struct BMPInfo), 1, file);
    image->width = info->width;
    image->height = info->height;
    image->data = malloc(image->height * image->width * sizeof(struct pixel));

//Считываем пиксели
    uint64_t padding = calculate_padding(image->width);

    for (size_t i = 0; i < image->height; i++) {
        fread( &(image->data[i*image->width]), sizeof(struct pixel), image->width, file);
        fseek(file, (long) padding, SEEK_CUR);
}
    fclose(file);
    return 0;
}


//Запись в файл
int write_data_in_file(const char* output_file_path, struct BMPInfo* info, struct image* image) {
    FILE* output_file = fopen(output_file_path, "wb");
    if (output_file == NULL) {
        return 1;
    }
    uint64_t padding = calculate_padding(image->width);
    if(!fwrite(info, sizeof(struct BMPInfo), 1, output_file)){
        return 1;
    }

    for (size_t j = 0; j < image->height; j++){
        if(!fwrite(&image->data[j*image->width], image->width*sizeof(struct pixel), 1, output_file)){
            return 1;
        }

        for(size_t i = 0; i < padding; i++){
            fputc(0, output_file);
        }
    }

    fclose(output_file);
    return 0;
}