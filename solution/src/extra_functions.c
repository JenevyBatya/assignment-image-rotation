
#include "../include/extra_functions.h"
#include <stdlib.h>
//Компилится хорошо, пока нареканий нет(скорее всего, ошибка где-то здесь)
//Функция поворота картинки, принимает заполненный объект первоначальной картинки и пустой для новой картинки
int rotate(struct image* image, struct image* imageOut, struct BMPInfo* info, struct BMPInfo* infoForNew) {
//меняем местами ширину и высоту, создаем в стеке новое пространство для смены пикселей
    imageOut->width = image->height;
    imageOut->height = image->width;
    imageOut->data = (struct pixel*)malloc(imageOut->height * imageOut->width * sizeof(struct pixel));
    if(!imageOut->data) return 1;

    for (size_t j = 0; j < imageOut->height; j++) {
        for (size_t i = 0; i < imageOut->width; i++) {
            imageOut->data[j * imageOut->width + i] = image->data[(image->height - i - 1) * image->width + j];
        }
    }
    *infoForNew = *info;
    infoForNew->height = imageOut->height;
    infoForNew->width = imageOut->width;

    return 0;
}

uint64_t calculate_padding(uint64_t width){
    uint64_t bytes_in_row = width* sizeof(struct pixel);
    return (4 - (bytes_in_row % 4))%4;
}

