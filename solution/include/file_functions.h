#ifndef FILE_FUNCTIONS
#define FILE_FUNCTIONS
#include "structs.h"

int read_BMP_file(const char* input_file_path, struct BMPInfo* info, struct image* image);
int write_data_in_file(const char* output_file_path, struct BMPInfo* info, struct image* image);

#endif