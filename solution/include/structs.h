#ifndef STRUCTS_H
#define STRUCTS_H

#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct  __attribute__((packed)) BMPInfo {
        uint16_t signature;
    uint32_t fileSize;
    uint32_t reserved;    
    uint32_t dataOffset;  
    uint32_t infoSize;      
    uint32_t width;         
    uint32_t height;        
    uint16_t planes;        
    uint16_t bitCount;      
    uint32_t compression;   
    uint32_t imageSize;     
    int32_t xPixelsPerM;  
    int32_t yPixelsPerM;  
    uint32_t colorsUsed;    
    uint32_t colorsImportant; 
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

#endif